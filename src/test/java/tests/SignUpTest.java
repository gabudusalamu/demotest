package tests;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

/**
 * Simple test for the CI/CD demo purpose 
 * during the test, located element is highlighted for 
 * visibility purpose for the viewer. 
 * @author GAbudusalamu
 *
 */
public class SignUpTest {
	
	static private WebDriver driver;
	static private String testSiteURL ="";
	// http://uscis-application-sandbox.epqhqmrbks.us-east-1.elasticbeanstalk.com/

	@BeforeClass
	public static void setUp(){
		String testSiteURL = System.getProperty("entry");
		System.out.println("Test Started");
		// Before any test cases are executed
		// webdriver instance will be initiated 
		//String driverPath = System.getProperty("user.dir") + "\\src\\test\\resources\\driver\\chromedriver.exe"; // for windows
		String driverPath = System.getProperty("user.dir") + "/src/test/resources/driver/linux/chromedriver";
		System.out.println(driverPath);
		System.setProperty("webdriver.chrome.driver", driverPath);
		ChromeOptions options = new ChromeOptions();  
		options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors");  
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get(testSiteURL);
		System.out.println("At test site: " + testSiteURL);
	}
	
	@Test
	public void signup_test()
	{
		// ---- Test Data ---- // 
		String name = "Tom Hanks";
		String email = "test_fake_email@gmail.com";
		
		// ---- Test Steps ---- // 
		// Step 1: click  the "Sign Up Today" button 
		WebElement signUpTodayButton = findElementHighlight(By.xpath("//a[text()='Sign up today']"));
		signUpTodayButton.click();
		System.out.println("Clicked 'Sign Up Today' button");
		waitFor(1);     // <-- do not delete this, since we have to wait for pop-up
		
		// Step 2: Enter the name 
		WebElement nameInput = findElementHighlight(By.xpath("//input[@id='name']"));
		nameInput.sendKeys(name);
		System.out.println("Enter the name: " + name);
		
		// Step 3: Enter the email 
		WebElement emailInput = findElementHighlight(By.xpath("//input[@id='email']"));
		emailInput.sendKeys(email);
		System.out.println("Enter the email: " + email);
		
		// Step 4: From the dropdown, select "NO"
		WebElement dropdownElem = findElementHighlight(By.tagName("select"));
		dropdownElem.click();
		Select dropdown = new Select(dropdownElem);
		dropdown.selectByIndex(0);
		System.out.println("From the drop-down selected 'No'");
		
		// Step 5: Click the "Sign Up!" button
		waitFor(1);
		WebElement signUpButton = findElementHighlight(By.id("signup"));
		waitFor(1);
		signUpButton.click();
		System.out.println("Clicked 'Sign Up!' button.");
		
		// ---- Test Assertion ---- // 
		WebElement resultBanner = findElementHighlight(By.id("signupErrorText"));
		boolean result = resultBanner.isEnabled();
		waitFor(2);
		Assert.assertTrue(result);
		
	}
	
	// freezes the current thread for specified second
	private void waitFor(int second) {
		try{
			Thread.sleep(second * 1000);
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Wait Error");
		}
	}
	
	// this method wraps the existing fineElement method of driver
	// it highlights the elemen located for demo purpose
	private WebElement findElementHighlight(By by) {
		WebElement elem = driver.findElement(by);
		//draw a border around the found element for highlight effect
		if(driver instanceof JavascriptExecutor) {
			String script = "arguments[0].style.border='3px solid #FF8C00'";  //gold yellow color
			((JavascriptExecutor)driver).executeScript(script, elem);
			waitFor(1);
		}
		return elem;
	}
	
	@AfterClass
	public static void tearDown() {
		System.out.println("Test Ended");
		if(driver != null) {
			driver.close();
			driver.quit();
		}
	}
}
